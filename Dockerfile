FROM alpine:3.12
RUN \
	apk --no-cache update && apk add --no-cache \
	bash \
	libc6-compat \
	rsyslog \
	rsyslog-relp \
	&& adduser -s /bin/ash -D rsyslog rsyslog \
	&& echo "rsyslog ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
	&& wget -q https://github.com/negbie/fancy/releases/download/1.6/fancy -O /opt/fancy \
	&& chmod +x /opt/fancy

COPY	files/rsyslog.conf /etc/rsyslog.conf
CMD	["/usr/sbin/rsyslogd","-n"]
